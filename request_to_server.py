import requests
import json
url_reqbin = 'http://requestb.in/16vo0v91'
id = "11483427936188"
import time

def send_cv_result(result_dict, base_link):
    '''
    Send map image-mac result to server.
    :param result_dict: dict map
    :return: response of http request
    <response200>
    '''
    # url = 'https://api-server-koa.herokuapp.com/api/result'
    # url = https://sleepy-hollows-65143.herokuapp.com/api/result/reg_to_client

    url = base_link+"/api/result/reg_to_client"
    print (url)
    result_data = []
    for key, value in result_dict.items():
        data_unit = {"personId": key, "imageLink": value}
        result_data.append(data_unit)

    payload = {
        "unixtime":int(time.time()),
        "recognize_result":result_data
    }

    headers = {"Content-Type": "application/json"}
    payloadjs = json.dumps(payload)
    response = requests.post(url, data=payloadjs, headers=headers)
    return response



def test_cv_result():
    '''
    run send_cv_result with mock data
    :return:
    '''
    result_dict = dict()
    result_dict["cat"] = "asdasdasdasdasdasdasd"
    result_dict["dog"] = "www.dog.com"
    res = send_cv_result(result_dict, "https://sleepy-hollows-65143.herokuapp.com")
    print (res)

# test_cv_result()

