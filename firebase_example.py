import os
import re
from os import listdir
from appJar import gui
import pyrebase
import subprocess

#py_path = '/home/vdvinh/anaconda3/bin/python'
py_path = 'python'
import imgur_download
from request_to_server import *
import result_gui
import sys

SHOW_GUI_TRIGGER = "meow_gui"


# all the link and api
BASE_LINK_VIETTEL = "http://125.212.233.106:3000/"
BASE_LINK_HEROKU_TESTCV = "https://sleepy-hollows-65143.herokuapp.com/"
BASE_LINK_LOCAL =  "http://127.0.0.1:3000/"
config_integrate = {
    "apiKey": "AIzaSyBNeJcW6CKp9v7z9HjRJHcOj7Sw7qY14AM",
    "authDomain": "api-server-koa.firebaseapp.com",
    "databaseURL": "https://api-server-koa.firebaseio.com",
    "storageBucket": "api-server-koa.appspot.com",
    "messagingSenderId": "409014105383"
}

config_test = {
    "apiKey": "AIzaSyAyLRrTVdNStBd0BZokcGdUBFvHLn9NTd8",
    "authDomain": "testcv-a3792.firebaseapp.com",
    "databaseURL": "https://testcv-a3792.firebaseio.com",
    "storageBucket": "testcv-a3792.appspot.com",
    "messagingSenderId": "258711105674"
}

# config and link in-use
config = config_test
BASE_LINK = BASE_LINK_HEROKU_TESTCV

counter = 0
startClassify = 1000
firebase = pyrebase.initialize_app(config)


def match_mac_image(cur_dir):
    '''
       process cv clustering here
       :param cur_dir: directory
               macs.txt
               1.png
               2.png
       :return:
       an array of macs
       macs[0] is mac address of 0.png in cur_dir
       '''

    ### example
    # some cv run here to match image to macs
    ###
    return ['mac0forimage0', 'mac1forimage1', 'mac2', 'mac3']  # mock output of Vinh code
    pass

def test_stream_handle(message):
    print('event ', message["event"]) # put
    print('path ', message["path"]) # /-K7yGTTEp7O549EzTYtI
    print('data', message["data"]) # {'title': 'Pyrebase', "body": "etc..."}

def stream_handle(message):
    '''
    will be called when something new on firebase
    will be called when api/detect was called
    :param message:
    :return:
    '''
    show_gui = False

    print ('Stream handle')
    data = message["data"]
    path = message["path"]
    if (message['event']=='put'):  # skip the first one, old data (always put method when start)
        # we need event 'patch'
        return


    idDevice = path[1:]  # remove first character, the slash
    frameid = data["frameId"]
    print (idDevice)
    print (frameid)
    print (data)

    frame = data
    cur_dir = idDevice+'/'+frameid # the folder will save data
    print ('frame id ',cur_dir)
    os.makedirs(os.path.abspath(cur_dir))  # create new folder

    f = open(cur_dir + '/' + 'macs.txt', 'w')

    # write id device at top of macs file
    # idDevice = index.split('/')[0]
    f.write(idDevice + '\n')  # write idDevice on top of macs.txt
    macs_data = get_full_macs(idDevice)
    if (macs_data != None):
        for mac in macs_data:
            f.write(mac + '\n')
            print (mac)
        if (SHOW_GUI_TRIGGER in macs_data):
            show_gui = True

    f.close()
    if (frame["links"] != None):
        for idx, link in enumerate(frame["links"]):
            imgur_download.getImg(cur_dir, BASE_LINK + link, idx)

    macs_result = match_mac_image(cur_dir)
    response = send_cv_result(idDevice, macs_result, BASE_LINK)  # send the result to firebase for rashberry to listen
    print (response)  # <Response [200]> is success, check firebase for result
    print ('frame id ', cur_dir)  # looking for entry with frame id on firebase for result

    # show gui
    # result_gui.make_gui(frameid, macs_result, cur_dir)
##############################################
    arg1 = frameid
    arg2 = '\"' + ' '.join(macs_result) + '\"'
    arg3 = '\"' + cur_dir + '\"'
    command = py_path + ' result_gui.py' + ' ' + arg1 + ' ' + arg2 + ' ' + arg3
    print (command)
    if (show_gui):
        os.system(command)
################################################3

def get_full_macs(idDevice):
    """
    Request full macs of frame from server
    (because it only stream change data)
    :param idDevice:
    :return: list of macs
    """
    db = firebase.database()
    # my_stream = db.child("/upload").stream(stream_handler=stream_handle)
    macs =  db.child("/upload/"+idDevice+'/'+'macs').get().val()
    return macs

# macs_result = match_mac_image("/home/vdvinh/FaceNet/openface/demos/71483431485500")
# print macs_result
if __name__ == "__main__":
    db = firebase.database()
    my_stream = db.child("/upload").stream(stream_handler=stream_handle)

    print ('breakpoint')
