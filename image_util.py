import base64
import requests
import json
import sending_reg_to_client
#
# with open("image/img1.jpg", "rb") as imageFile:
#     str = base64.b64encode(imageFile.read())
#     print str
#
# fh = open("image/new_file.jpg", "wb")
# fh.write(str.decode('base64'))
# fh.close()

# def image_to_base64(image_directory):
#     with open(image_directory, "rb") as imageFile:
#         str = base64.b64encode(imageFile.read())
#         return str


def send_image(result, base_link):
    """Send data to server"""
    URL = base_link+'api/upload2/no_firebase'
    multipart_form_data = result
    for key in result:
        result[key] =  open(result[key], 'rb')

    response = requests.post(URL, files=multipart_form_data)
    result_json =  json.loads(response._content)
    result = result_json['links']

    return result

if __name__ == "__main__":
    # image_string = image_to_base64("image/new_file.jpg")
    result = send_image({"Pusheen":'image/img1.jpg', "Image2":"image/img2.jpg"}, "http://127.0.0.1:3000/")
    sending_reg_to_client.sending_reg_to_client(result, "http://127.0.0.1:3000/")
    print (result)
    # print (image_string)