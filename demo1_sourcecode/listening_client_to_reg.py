import CONFIG
import pyrebase
# config and link in-use
config = CONFIG.config_test
BASE_LINK = CONFIG.BASE_LINK_HEROKU_TESTCV

counter = 0
startClassify = 1000
firebase = pyrebase.initialize_app(config)

def client_to_reg_handle(message):
    '''
    :param message:
    :return:
    '''
    show_gui = False

    print ('Stream handle')
    data = message["data"]
    path = message["path"]
    unixtime = data['unixtime']
    chosen_id = data['personId']
    print(chosen_id)

def start_listening_client_to_reg():
    db = firebase.database()
    my_stream = db.child("result/ui/client_to_reg").stream(stream_handler=client_to_reg_handle)

start_listening_client_to_reg()