import requests
import json
import time
import image_util
def sending_reg_to_client(result_dict, base_link):
    '''
    send pair of person id _ imageLink to client
    {
        personId =
    }
    :param result_dict: dict map
    :return: response of http request
    <response200>
    '''

    # url = 'https://api-server-koa.herokuapp.com/api/result'
    # url = https://sleepy-hollows-65143.herokuapp.com/api/result/reg_to_client # for testing and dev

    url = base_link+"api/result/reg_to_client"
    print (url)
    result_data = []
    for personId, image_link in result_dict.items():
        data_unit = {"personId": personId, "imageLink": image_link}
        result_data.append(data_unit)

    payload = {
        "unixtime":int(time.time()),
        "recognize_result":result_data
    }

    headers = {"Content-Type": "application/json"}
    payloadjs = json.dumps(payload)
    response = requests.post(url, data=payloadjs, headers=headers)
    return response

if __name__=="__main__":
    result_dict = dict()
    result_dict["cat"] = "Pusheen"
    result_dict["dog"] = "Pluto"
    res = sending_reg_to_client(result_dict, "https://sleepy-hollows-65143.herokuapp.com")
    print (res)