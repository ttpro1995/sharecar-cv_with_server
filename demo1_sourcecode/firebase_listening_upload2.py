import os
import pyrebase
import image_util
import imgur_download
import CONFIG
import sending_reg_to_client

#py_path = '/home/vdvinh/anaconda3/bin/python'
py_path = 'python'


# config and link in-use
config = CONFIG.config_test
BASE_LINK = CONFIG.BASE_LINK_HEROKU_TESTCV

counter = 0
startClassify = 1000
firebase = pyrebase.initialize_app(config)



def match_mac_image(cur_dir):
    '''
       process cv clustering here
       :param cur_dir: directory
               macs.txt
               1.png
               2.png
       :return:
       an array of macs
       macs[0] is mac address of 0.png in cur_dir
       '''

    ### example
    # some cv run here to match image to macs
    ###


    return {'Pusheen':'image/img1.jpg', 'Meow':'image/img2.jpg'}  # mock output of Vinh code


def test_stream_handle(message):
    print('event ', message["event"]) # put
    print('path ', message["path"]) # /-K7yGTTEp7O549EzTYtI
    print('data', message["data"]) # {'title': 'Pyrebase', "body": "etc..."}


def stream_handle(message):
    '''
    will be called when something new on firebase
    will be called when api/detect was called
    :param message:
    :return:
    '''


    print ('Stream handle')
    data = message["data"]
    path = message["path"]
    print ('fullmessage ', message)
    if (data['personId'] is None):
        return

    personId = data['personId']


    cur_dir = personId # the folder will save data
    if (type(cur_dir) is list):
        cur_dir = cur_dir[0]
        if cur_dir == '':
            return

    cur_dir = CONFIG.IMAGE_DIR + '/' + cur_dir
    print ('cur_dir ',cur_dir)
    os.makedirs(os.path.abspath(cur_dir))  # create new folder

    f = open(cur_dir + '/' + 'macs.txt', 'w')

    # write id device at top of macs file
    # idDevice = index.split('/')[0]
    f.write(cur_dir + '\n')  # write idDevice on top of macs.txt
    if (data["links"] != None):
        for idx, link in enumerate(data["links"]):
            imgur_download.getImg(cur_dir, BASE_LINK + link, idx)

    #############################################################

    result = match_mac_image(cur_dir)
    result_with_link = image_util.send_image(result, BASE_LINK)

    response = sending_reg_to_client.sending_reg_to_client(result_with_link ,BASE_LINK)  # send the result to firebase for rashberry to listen
    # print (response)  # <Response [200]> is success, check firebase for result
    # print ('frame id ', cur_dir)  # looking for entry with frame id on firebase for result

    # show gui
    # result_gui.make_gui(frameid, macs_result, cur_dir)
##############################################


# macs_result = match_mac_image("/home/vdvinh/FaceNet/openface/demos/71483431485500")
# print macs_result
if __name__ == "__main__":
    db = firebase.database()
    my_stream = db.child("/upload2").stream(stream_handler=stream_handle)

    print ('breakpoint')
