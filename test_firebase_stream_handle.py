import pytest
from collections import *
import firebase_example
class TestFireBase:
    def test_firebase_streamhandle(self):
        message = {u'path': u'/', u'data': {u'testid': {u'macs': [u'tung', u'chi'], u'links': [u'images/bf8a8374-304e-4b6f-996d-27d0cfc53a9f/0_0.jpg', u'images/a5db9370-b2c4-49a7-b7a6-b5544438a2d9/0_1.jpg', u'images/c04d5218-347b-4dfc-bda4-71494c1eb812/0_2.jpg', u'images/47b9b1ac-5d5f-49dd-b975-c7fc63d2edae/1_0.jpg', u'images/f3dcb5ca-a229-40ae-97c8-f98437660006/1_1.jpg', u'images/918b56c6-0ac2-43ac-91b2-678e6d00f226/1_2.jpg', u'images/608bb50e-a133-47a2-a726-40b9d14f5966/2_0.jpg', u'images/19fdaf02-99b0-40ef-8eab-622db709df01/2_1.jpg', u'images/31d2cca6-b090-4034-8a1b-c48fb3e92516/2_2.jpg'], u'frameId': u'8e5d1dda-db46-4368-b7f6-421da3c7453b'}}, 'event': u'put'}
        firebase_example.stream_handle(message)
