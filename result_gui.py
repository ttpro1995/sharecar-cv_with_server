from appJar import gui
import glob
import sys
sys.path.append("./")
import os.path

def make_gui(frame_id, result_label, image_dir):
    # top slice - CREATE the GUI
    app = gui(frame_id, "1000x700")

    file_list = glob.glob(image_dir + '/*.jpg') # list of file

    idx = 0
    max = len(file_list)

    # create
    app.setSticky("nw")
    app.startLabelFrame('IMAGE', 0, 0)
    for i in range(0,3):
        for j in range(0,8):
            name = str(i)+str(j)
            app.startLabelFrame(name,i,j)
            if (idx < max):
                app.addImage(str(i)+str(j), file_list[idx])
                # app.addLabel(str(i)+str(j), result_label[idx])
            else:
                app.addImage(str(i)+str(j), "placeholder_cat.gif", row=0)
               #app.addLabel(str(i)+str(j),"My name" +str(i)+str(j), row=1)
            idx+=1
            app.stopLabelFrame()
    app.stopLabelFrame()

    list_of_name = []
    list_of_name.extend(result_label)
    app.startLabelFrame('MAC', 1, 0)
    app.addLabel('name_label', list_of_name)
    app.stopLabelFrame()

    # bottom slice - START the GUI
    app.go()





def test_make_gui():
    make_gui("test_frame_id", ['name a','name b','name c','name d','name e','name f','name g'], './testid/0ac71341-4e60-49bb-8650-d78097f10b74')

#test_glop('./testid/0ac71341-4e60-49bb-8650-d78097f10b74')
if __name__=="__main__":
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
    arg3 = sys.argv[3]
    arg2_list = arg2.split()
    make_gui(arg1, arg2_list, arg3)