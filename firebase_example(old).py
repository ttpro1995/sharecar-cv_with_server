import pyrebase
import os
import imgur_dl_old
from request_to_server import *

'''
config = {
    # config for firebase
    # Chanh should know what is it
   "apiKey": "AIzaSyBNeJcW6CKp9v7z9HjRJHcOj7Sw7qY14AM",
   "authDomain": "api-server-koa.firebaseapp.com",
   "databaseURL": "https://api-server-koa.firebaseio.com",
   "storageBucket": "api-server-koa.appspot.com",
   "messagingSenderId": "409014105383"
};
'''


config = {
    "apiKey": "AIzaSyBNeJcW6CKp9v7z9HjRJHcOj7Sw7qY14AM",
    "authDomain": "api-server-koa.firebaseapp.com",
    "databaseURL": "https://api-server-koa.firebaseio.com",
    "storageBucket": "api-server-koa.appspot.com",
    "messagingSenderId": "409014105383"
};

firebase = pyrebase.initialize_app(config) # init firebase with config

def match_mac_image(cur_dir):
    '''
    process cv clustering here
    :param cur_dir: directory
            macs.txt
            1.png
            2.png
    :return:
    an array of macs
    macs[0] is mac address of 0.png in cur_dir
    '''

    ### example
    # some cv run here to match image to macs
    ###
    return ['mac0forimage0', 'mac1forimage1','mac2', 'mac3'] # mock output of Vinh code
    pass


def test_stream_handle(message):
    print(message["event"]) # put
    print(message["path"]) # /-K7yGTTEp7O549EzTYtI
    print(message["data"]) # {'title': 'Pyrebase', "body": "etc..."}



def stream_handle(message):
    '''
    will be called when something new on firebase
    will be called when api/detect was called
    :param message:
    :return:
    '''

    data = message["data"]
    index = message["path"]
    if (index == '/'): # skip the first one (which is none or old data)
        return
    index = index[1:] # remove first character
    print (index)
    print (data)

    frame = data
    cur_dir = str(index)
    os.makedirs(cur_dir) # create new folder

    f = open(cur_dir+'/'+'macs.txt', 'w')

    # write id device at top of macs file
    idDevice = frame['idDevice']
    f.write(idDevice+'\n') # write idDevice on top of macs.txt
    for mac in frame["macs"]:
        f.write(mac+'\n')
        print (mac)
    f.close()
    for idx, link in enumerate(frame["links"]):
        imgur_dl_old.getImg(cur_dir, link, idx)

    macs_result = match_mac_image(cur_dir)
    response = send_cv_result(cur_dir, macs_result) # send the result to firebase for rashberry to listen
    print (response) # <Response [200]> is success, check firebase for result
    print ('frame id ',cur_dir ) # looking for entry with frame id on firebase for result



db = firebase.database()
my_stream = db.child("/upload").stream(stream_handler=stream_handle) # listening to firebase event
# my_stream = db.child("/upload").stream(stream_handler=test_stream_handle) # listening to firebase event


print ('breakpoint') # ready to listen to firebase event
# call api/detect after you see 'breakpoint' on python console